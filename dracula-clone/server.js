const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const fs = require('fs');
const fetch = require('node-fetch');

function shuffle(a) {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

// GLOBALS
let programs;
let program_names = [];

fs.readFile('res/progs.json', (err, data) => {
    if (err) {
        console.error(err)
        return
    }
    programs = JSON.parse(data);

    for (let prog in programs) {
        program_names.push(programs[prog].name);
    }

    program_names = shuffle(program_names);

})

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

// create a GET route
app.get('/express_backend', (req, res) => {
    res.send({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT' });
});

app.get('/get_progs', (req, res) => {
    res.send(shuffle(program_names));
});

app.get('/github_info', async (req, res) => {
    let response = await fetch('https://api.github.com/search/repositories?q=dracula-theme');
    let json = await response.json();
    let stars = await json.items[0].stargazers_count;
    let forks = await json.items[0].forks_count;

    res.send(`${stars}|${forks}`)
});

app.get('/progs_data', (req, res) => {
    res.send(programs);
})