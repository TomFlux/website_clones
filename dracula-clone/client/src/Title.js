import React, { Component } from 'react';
import CountUp from 'react-countup';
import "./stylesheets/title.css";
import dracula from './res/imgs/dracula.svg';

class Title extends Component {

    constructor(props) {
        super(props);

        this.state = {
            stars: 0,
            forks: 0,
            countUps: null,
        }
    }

    async componentDidMount() {

        let githubinfo = await (await this.getGitHub()).split('|');

        this.setState({
            stars: await Number(await githubinfo[0]),
            forks: await Number(await githubinfo[1]),
        }, async () => {
            this.setState({
                countUps: [
                    <div key='forks' className='forksandstars' >
                        <CountUp key='forksCount' end={await this.state.forks} />
                        <p key='forkText'> forks </p>
                    </div>,
                    <div key='starts' className='forksandstars'>
                        <CountUp key='starsCount' end={await this.state.stars} />
                        <p key='starsText'> stars </p>
                    </div>
                ]
            })
        });

        let text = document.getElementById('typeText');
        let programs = await (await fetch('/get_progs')).json();
        let currIndex = 0;
        let currChar = 0;
        let reverse = true;

        let waiting = false;
        let waitingCount = 0;
        let maxWaitingCount = 30;

        setInterval
            (async () => {
                if (waiting) {
                    if (waitingCount === maxWaitingCount) {
                        waitingCount = 0;
                        waiting = false;
                    } else {
                        waitingCount++;
                    }
                } else {
                    if ((currChar - 1) === await programs[currIndex].length) {
                        reverse = true;
                        waiting = true;
                    }
                    else if (currChar === 0) {
                        reverse = false;
                        if ((currIndex + 1) === await programs.length) {
                            programs = await (await fetch('/get_progs')).json();
                            currIndex = 0;
                        }
                        else
                            currIndex++;
                    }

                    text.innerHTML = await " " + programs[currIndex].substring(0, currChar);

                    reverse ? currChar-- : currChar++;
                }
            }, 50);
    }

    async getNewProg() {
        const response = await fetch('/new_prog');
        const body = await response.blob();

        if (response.status !== 200) {
            throw Error(body.message)
        }
        return body.text();
    }

    async getGitHub() {
        const response = await fetch('/github_info');
        const body = await response.blob();

        if (response.status !== 200) {
            throw Error(body.message)
        }
        return body.text();
    }

    render() {
        return (
            <div id='container'>
                <img key='draculalogo' src={dracula} alt="dracula logo" width='220px' height='220px' />
                <p key='title' id='title'>Dracula</p>
                <div key='typingtext' id='draculaThemeFor'>
                    <p>Dracula theme for</p><p id='typeText' />
                </div>
                <div key='githubinfo' id='githubInfo'>
                    {this.state.countUps}
                </div>
            </div>
        );
    }
}

export default Title;