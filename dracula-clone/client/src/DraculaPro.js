import React, { Component } from 'react';
import './stylesheets/draculapro.css'

const Box = () => {
    return <div id='box'>
        <p id='boxText1'>the new theme is here!</p>
        <p id='boxText2'>Meet the premium version with tons of goodies.</p>
        <p id='boxText3'>Take me there</p>
    </div>;
}

class DraculaPro extends Component {
    render() {
        return <div id='dpContainer'>
            <h1 id='dpTitle'>Dracula PRO</h1>
            <Box />
        </div>
    }
}

export default DraculaPro