import React, { Component } from 'react';
import './stylesheets/searchbar.css'

class Program extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hovering: false,
        }
    }

    render() {
        return (
            <div className='progContainer'
                style={
                    this.props.visible === 'visible'
                        ? {
                            transform:
                                `translate(${(this.props.num % 3 * 360) - 75}px, ${(Math.floor((this.props.num / 3)) * this.props.spacing[1])}px) scale(1)`
                        } : { transform: `scale(0)`, }}

                onMouseEnter={() => this.setState({ hovering: true })}
                onMouseLeave={() => this.setState({ hovering: false })}>
                <span className='progImgContainer'>
                    <img
                        className='progImage'
                        alt={this.props.name}
                        src={require(`./res/imgs/${this.props.src}`)}
                    />
                </span>
                <p
                    className='progText' style={
                        this.state.hovering
                            ? {
                                color: this.props.color,
                            } : {
                                color: 'white',
                            }
                    }
                >{this.props.name}</p>
            </div >
        );
    }
};

class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            progEls: [],
            elsToRender: [],
            spaceX: 350,
            spaceY: 350,
            currentlyFocused: 'all',
        }
    }

    async componentDidMount() {
        let res = await (await fetch('/progs_data')).json();
        this.setState({
            progEls: res,
            elsToRender: res,
        });
    }

    getEls(focus = null) {
        // setState does not make changes until the next frame,
        // so if you want 'instant' change you must pass it in a 
        // parameter
        let focused = focus === null ? this.state.currentlyFocused : focus

        let e = [];

        for (let i = 0; i < this.state.progEls.length; i++) {
            let currEl = this.state.progEls[i];
            if (
                currEl.name.toLowerCase().includes(document.getElementById('searchBox').value.toLowerCase()) &&
                (
                    focused === 'all' ||
                    currEl.supported.includes(focused) ||
                    currEl.supported.includes('all')
                )
            ) {
                e.push(this.state.progEls[i])
            }
        }
        this.setState({ elsToRender: e });
    }

    render() {
        let count = -1;

        return <div id='background' style={
            this.state.elsToRender.length === 0
                ? { height: '200px' }
                : { height: `${((Math.floor(this.state.elsToRender.length / 3) + 1) * 350) + 200}px` }
        }>
            <div id='searchArea'>
                <p key='numProrgs'> Showing {this.state.elsToRender.length} app(s)</p>
                <input
                    key='searchBox'
                    id='searchBox'
                    type='text'
                    placeholder='Search..'
                    onChange={() => {
                        this.getEls();
                    }}
                />
                <div key='searchButtons' id='searchButtons'>
                    <button
                        key='focusAll'
                        style={{
                            borderTopLeftRadius: '4px',
                            borderBottomLeftRadius: '4px',
                            borderLeft: '1px solid #bd93f9',
                            borderRight: '1px solid #bd93f9',
                            backgroundColor: this.state.currentlyFocused === 'all'
                                ? '#bd93f9' : null,
                            color: this.state.currentlyFocused === 'all'
                                ? 'black' : 'white',
                        }}
                        onClick={() => {
                            this.setState({ 'currentlyFocused': 'all' });
                            this.getEls('all');
                        }}>
                        All
                    </button>
                    <button
                        key='focusLinux'
                        style={{
                            borderRight: '1px solid #bd93f9',
                            backgroundColor: this.state.currentlyFocused === 'linux'
                                ? '#bd93f9' : null,
                            color: this.state.currentlyFocused === 'linux'
                                ? 'black' : 'white',
                        }}
                        onClick={() => {
                            this.setState({ 'currentlyFocused': 'linux' });
                            this.getEls('linux');
                        }}>
                        Linux
                    </button>
                    <button
                        key='focusMac'
                        style={{
                            borderRight: '1px solid #bd93f9',
                            backgroundColor: this.state.currentlyFocused === 'mac'
                                ? '#bd93f9' : null,
                            color: this.state.currentlyFocused === 'mac'
                                ? 'black' : 'white',
                        }}
                        onClick={() => {
                            this.setState({ 'currentlyFocused': 'mac' });
                            this.getEls('mac');
                        }}>
                        Mac
                    </button>
                    <button
                        key='focusWindows'
                        style={{
                            borderTopRightRadius: '4px',
                            borderBottomRightRadius: '4px',
                            borderRight: '1px solid #bd93f9',
                            backgroundColor: this.state.currentlyFocused === 'windows'
                                ? '#bd93f9' : null,
                            color: this.state.currentlyFocused === 'windows'
                                ? 'black' : 'white',
                        }}
                        onClick={() => {
                            this.setState({ 'currentlyFocused': 'windows' });
                            this.getEls('windows');
                        }}>
                        Windows
                    </button>
                </div>
            </div>
            <div id='programContainer'>
                {this.state.progEls.map((prog) => {
                    let r = this.state.elsToRender.includes(prog) ? true : false;
                    return <Program
                        key={prog.name}
                        name={prog.name}
                        src={prog.img}
                        color={prog.color}
                        num={r ? ++count : -1}
                        spacing={[this.state.spaceX, this.state.spaceY]}
                        visible={r ? 'visible' : 'hidden'} />
                })}
            </div>
        </div >;
    }
}

export default SearchBar;