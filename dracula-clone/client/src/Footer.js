import React, { Component } from 'react';
import './stylesheets/footer.css'

class Footer extends Component {
    componentDidMount() {
        document.getElementById('heart').addEventListener('mouseenter', () => {
            document.getElementById('heart').classList.add('heartAnimate');
        });
    }

    render() {
        return <div id='footerCont'>
            <p>Made with <span id='heart'>♥</span> by <span id='author'> Zeno Rocha</span></p>
            <p>under <span id='license'>MIT license</span></p>
        </div>;
    }
}

export default Footer;