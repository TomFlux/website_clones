import React from 'react';
import './stylesheets/app.css';

import Navbar from './Navbar.js';
import Title from './Title.js';
import ProgramSearch from './SearchBar.js';
import DraculaPro from './DraculaPro.js';
import Footer from './Footer.js';

function App() {
  return (
    <React.StrictMode>
      <header>
        <Navbar />
        <Title />
      </header>
      <div>
        <ProgramSearch />
        <DraculaPro />
      </div>
      <footer>
        <Footer />
      </footer>
    </React.StrictMode>

  );
}

export default App;
