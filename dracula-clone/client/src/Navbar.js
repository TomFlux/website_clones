import React from 'react';
import "./stylesheets/navbar.css"

function Navbar() {
    return (
        <nav className='container'>
            <div className='leftContent'>
                Dracula
            </div>
            <div className='rightContent'>
                <p className='rightText'> About </p>
                <p className='rightText'> Contribute </p>
                <p className='draculaProText'> Dracula PRO</p>
            </div>
        </nav>
    );
}

export default Navbar;
