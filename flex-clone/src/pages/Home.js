import React from "react";

import "../index.css";

import book from "../res/imgs/book.png";
import buy from "../res/imgs/buy.png";
import bottom from "../res/imgs/bottom.jpg";

import gym1 from "../res/imgs/gym1.jpg";
import gym2 from "../res/imgs/gym2.jpg";
import gym3 from "../res/imgs/gym3.jpg";
import gym4 from "../res/imgs/gym4.jpg";

import appStore from "../res/imgs/appstore.png";
import playStore from "../res/imgs/playstore.png";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram, faFacebookF } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import {
  Map,
  TileLayer,
  Marker,
  Popup,
  Polygon,
  Polyline,
} from "react-leaflet";

class BottomInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      infoL: ["StudioFlex", "Ipswich, Suffolk", "info@studioflex.co.uk"],
      infoR: ["Studio Rules", "FAQ", "Contact"],
    };
  }
  render() {
    return (
      <div key="leftText" className="bottomInfo">
        <div>
          {this.state.infoL.map((text, idx) => {
            let colour;
            if (idx === 2) colour = "rgba(78,196,208,1)";
            else colour = "black";

            return (
              <div key={text} className="bottomText" style={{ color: colour }}>
                {text}
              </div>
            );
          })}
        </div>
        <div key="rightText" style={{ float: "right", textAlign: "right" }}>
          {this.state.infoR.map((text) => {
            return (
              <div key={text} className="bottomText">
                {text}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

class Contact extends React.Component {
  render() {
    return (
      <div className="contact">
        <div key="Infomation">
          <p className="heavy"> Studio Flex </p>
          <p>
            Three Rivers Business Center
            <br />
            Felixstowe Road
            <br />
            Suffolk
            <br />
            IP10 0BF
          </p>
          <p>01473 659126</p>
          <p>
            Got a question?
            <span style={{ textDecoration: "underline" }}>Get in Touch</span>
          </p>
          <p className="heavy">
            Don’t forget to
            <br />
            Download our App
          </p>
        </div>
        <div>
          <img src={playStore} alt="Play Store download" />
          <img src={appStore} alt="App Store download" />
        </div>
        <div key="Contact Link" className="contactIcons">
          {[
            [faInstagram, "instagram"],
            [faFacebookF, "facebook"],
            [faEnvelope, "email"],
          ].map((icon) => {
            return (
              <div className="contactIcon">
                <FontAwesomeIcon icon={icon[0]} key={icon[1]} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

class ContactMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 52.027588,
      lng: 1.239206,
      zoom: 15,
      polygon: [
        [52.026715, 1.23717],
        [52.026273, 1.238538],
        [52.027686, 1.241446],
        [52.028121, 1.239375],
      ],
      polyline1: [
        [52.026121, 1.238178],
        [52.027811, 1.228301],
        [52.032089, 1.218166],
        [52.035046, 1.210608],
        [52.037052, 1.203136],
        [52.042384, 1.198069],
        [52.047822, 1.18129],
      ],
      polyline2: [
        [52.026121, 1.238178],
        [52.023058, 1.246908],
        [52.025118, 1.254294],
        [52.011913, 1.279115],
        [52.00626, 1.28341],
        [51.997488, 1.300501],
      ],
      polyline3: [
        [52.026121, 1.238178],
        [52.027955, 1.245554],
        [52.026239, 1.254786],
        [52.035269, 1.259023],
        [52.046742, 1.275695],
        [52.059409, 1.276694],
      ],
    };
  }

  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <div className="contactMap">
        <Contact />
        <Map
          center={position}
          zoom={this.state.zoom}
          style={{ height: "760px" }}
        >
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position}>
            <Popup>Flex Studio</Popup>
          </Marker>
          <Polygon color="purple" positions={this.state.polygon} />
          <Polyline
            color="#1A237E"
            positions={this.state.polyline1}
            weight="10"
          />
          <Polyline
            color="#0288D1"
            positions={this.state.polyline2}
            weight="10"
          />
          <Polyline
            color="#1A237E"
            positions={this.state.polyline3}
            weight="10"
          />
        </Map>
      </div>
    );
  }
}

class Social extends React.Component {
  render() {
    return (
      <div className="social">
        <p>
          <FontAwesomeIcon icon={faInstagram} /> find out the latest news and
          offers @studioflexboutiquefitness
        </p>
      </div>
    );
  }
}

class Quotes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      quotes: [
        {
          person: "Hilary Duff",
          quote:
            "I think Pilates is really important to helping  me stay long and lean. I try to do it three times a week. I also do circuit training.",
        },
        {
          person: "Kate Hudson",
          quote:
            "I was 19 when I discovered Pilates, and I’m still doing it. It’s all about alignment, elongating your spine and strengthening your core. It makes me feel my strongest. Pilates is always challenging.",
        },
        {
          person: "Jennifer Aniston",
          quote:
            "I’m a Reformer Pilates person. it’s great. I had a chronic back, a pinched nerve and a hip problem and it’s completely solved all of it. I love it. It makes me feel like i’m taller",
        },
        {
          person: "David Beckham",
          quote:
            "I do an hour of pilates a day, it is fantastic and fitness-wise i am the best i have been for a long time. I am in the best shape in years.",
        },
      ],
      currentQuote: 0,
      changeQuote: setInterval(this.autoChangeQuote.bind(this), 7000),
    };
  }

  autoChangeQuote() {
    if (this.state.currentQuote + 1 > this.state.quotes.length - 1)
      this.changeQuote(0);
    else this.changeQuote(this.state.currentQuote + 1);
  }

  changeQuote(i) {
    let quoteEl = document.getElementsByClassName("quote")[0];

    if (quoteEl === undefined) return;

    clearInterval(this.state.changeQuote);
    this.setState({
      changeQuote: setInterval(this.autoChangeQuote.bind(this), 7000),
    });

    quoteEl.style.transform = "translateX(-100%)";

    setTimeout(() => {
      this.setState({
        currentQuote: i,
      });
      quoteEl.style.visibility = "hidden";

      quoteEl.style.transform = "translateX(100%)";

      setTimeout(() => {
        quoteEl.style.visibility = "visible";
        quoteEl.style.transform = "translateX(0%)";
      }, 1000);
    }, 1000);
  }

  render() {
    return (
      <div className="quoteContainer">
        <div className="quote">
          <p>"{this.state.quotes[this.state.currentQuote].quote}"</p>
          <p style={{ fontWeight: 700 }}>
            {this.state.quotes[this.state.currentQuote].person}
          </p>
        </div>
        <div className="quoteButtons">
          {this.state.quotes.map((quote, idx) => {
            let alpha;
            if (idx === this.state.currentQuote) {
              alpha = 1;
            } else {
              alpha = 0.4;
            }
            return (
              <div
                style={{ backgroundColor: `rgba(255, 255, 255, ${alpha})` }}
                key={quote.person}
                onClick={() => this.changeQuote(idx)}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

class BookSession extends React.Component {
  render() {
    return (
      <div
        style={{
          height: 735,
          backgroundColor: "#f5f5f5",
          marginTop: 15,
          width: "100%",
        }}
      />
    );
  }
}

class GymImages extends React.Component {
  render() {
    if (window.innerWidth > 1000) {
      return (
        <div className="gymImages">
          <div style={{ backgroundImage: `url(${gym1})` }} />
          <div style={{ backgroundImage: `url(${gym2})` }} />
          <div style={{ backgroundImage: `url(${gym3})` }} />
          <div style={{ backgroundImage: `url(${gym4})`, marginRight: `0` }} />
        </div>
      );
    } else return null;
  }
}

class BookAndBuy extends React.Component {
  render() {
    return (
      <div className="bnb">
        <div style={{ marginRight: "15px" }}>
          <img alt="book" src={book} />
        </div>
        <div>
          <img alt="buy" src={buy} />
        </div>
      </div>
    );
  }
}

class Promotion extends React.Component {
  render() {
    return (
      <div
        className="promotion"
        style={{
          backgroundColor: this.props.info.backgroundColour,
          color: this.props.info.textColour,
          marginRight: this.props.info.marginRight,
        }}
      >
        <div>
          <p className="promotionTop promotionText"> {this.props.info.top} </p>
          <p className="promotionPromo promotionText">
            {this.props.info.promo}
          </p>
          <p className="promotionFlavour promotionText">
            {this.props.info.flavour1} <br /> {this.props.info.flavour2}
          </p>
          <div className="promotionBuy promotionText">
            <div
              className="promotionBuyBackground"
              style={{
                backgroundColor: this.props.info.buyColor,
              }}
            >
              <span> Buy Now </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class Promotions extends React.Component {
  render() {
    return (
      <div className="promotions">
        {[
          {
            top: "TryFlex",
            promo: "£10",
            flavour1: "Any Class",
            flavour2: "(NEW clients only)",
            backgroundColour: "#f5f5f5",
            textColour: "black",
            buyColor: "rgb(77, 196, 207)",
            marginRight: "15px",
          },
          {
            top: "Early bird",
            promo: "50% off",
            flavour1: "All class packs",
            flavour2: "use code FLEX50 at checkout",
            backgroundColour: "#4dc4cf",
            textColour: "black",
            buyColor: "rgb(114, 128, 138)",
            marginRight: "15px",
          },
          {
            top: "2 Weeks Unlimited",
            promo: "£49",
            flavour1: "14 days ulimited classses",
            flavour2: "(NEW customers only)",
            backgroundColour: "#72808a",
            textColour: "white",
            buyColor: "rgb(77, 196, 207)",
            marginRight: "0px",
          },
        ].map((offer, num) => {
          return <Promotion key={`Promotion${num}`} info={offer} />;
        })}
      </div>
    );
  }
}

export default class HomePage extends React.Component {
  render() {
    return (
      <div className="mainBody">
        <Promotions />
        <BookAndBuy />
        <GymImages />
        <BookSession />
        <Quotes />
        <div
          style={{ backgroundImage: `url(${bottom})` }}
          className="bottomImage"
        />
        <Social />
        <ContactMap />
        <BottomInfo />
      </div>
    );
  }
}
