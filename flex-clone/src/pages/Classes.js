import React from "react";

import "../index.css";

import classText from "../res/text/classes.json";

const Class = (props) => {
  return this.props.text;
};

class ClassGroup extends React.Component {
  render() {
    return <p>{this.props.text}</p>;
  }
}

export default class Classes extends React.Component {
  render() {
    console.log(classText["Aerial Classes"]);
    return (
      <div class="mainBody">
        {classText.map((text) => {
          return <ClassGroup text={text.group} />;
        })}
      </div>
    );
  }
}
