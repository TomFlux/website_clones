import React from "react";
import "../index.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram, faFacebookF } from "@fortawesome/free-brands-svg-icons";

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cookiesAccepted: false,
    };
  }

  acceptCookies() {
    document.getElementsByClassName("cookies")[0].style.visibility = "hidden";
    this.setState({ cookiesAccepted: true });
    // send request to backend to collect cookies
  }

  render() {
    return (
      <div>
        {!this.state.cookiesAccepted ? (
          <div
            className="cookies footer"
            key="cookies"
            style={{ backgroundColor: "rgb(241, 241, 241)" }}
          >
            <div className="cookiesLeft">
              We use cookies to enhance your experience. By continuing to visit
              this site you agree to our use of cookies.
              <span> More info</span>
            </div>
            <div
              className="cookiesRight"
              onClick={() => {
                this.acceptCookies();
              }}
            >
              <span>Got it!</span>
            </div>
          </div>
        ) : (
          <div className="footer footerText">
            <span>© STUDIO FLEX. ALL RIGHTS RESERVED. </span>
            <div className="footerSocials">
              <FontAwesomeIcon icon={faInstagram} key="Footer Instagram" />
              <FontAwesomeIcon icon={faFacebookF} key="Footer Facebook" />
            </div>
          </div>
        )}
      </div>
    );
  }
}
