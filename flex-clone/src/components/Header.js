import React from "react";
import { Link } from "react-router-dom";

import "../index.css";

import logo from "../res/imgs/header-logo.png";

const Dropdown = (props) => {
  return (
    <div
      style={{ visibility: `hidden`, marginTop: `180px` }}
      className="dropdown"
      onMouseEnter={props.mouseEvents}
      onMouseLeave={props.mouseEvents}
    >
      <Link className="dropdownText" to={`FAQ`}>
        FAQ
      </Link>
      <br />
      <Link className="dropdownText" to={`studiorules`}>
        Studio Rules
      </Link>
    </div>
  );
};

class HeaderItem extends React.Component {
  render() {
    if (this.props.value === "")
      return <div className="topBarItem"> {this.props.value} </div>;
    else {
      return (
        <Link
          onMouseEnter={this.props.mouseEvents}
          onMouseLeave={this.props.mouseEvents}
          className="topBarItem"
          to={`/${this.props.value}`}
        >
          {this.props.value}
        </Link>
      );
    }
  }
}

export default class Header extends React.Component {
  handleHover() {
    let dropdown = document.getElementsByClassName("dropdown")[0];

    if (dropdown.style.visibility === `hidden`) {
      dropdown.style.marginTop = `120px`;
      dropdown.style.visibility = `visible`;
      dropdown.style.opacity = 1;
    } else {
      dropdown.style.marginTop = `180px`;
      dropdown.style.visibility = `hidden`;
      dropdown.style.opacity = 0;
    }
  }

  render() {
    return (
      <div className="header">
        <div className="topBarContainerOuter">
          <div className="topBarContainerInner">
            {[
              "about",
              "book",
              "buy",
              "shop",
              "LOGO",
              "café",
              "classes",
              "contact",
              "",
            ].map((phrase) => {
              if (phrase !== "LOGO") {
                let mouseEvents;

                phrase === "about" && (mouseEvents = () => this.handleHover());
                return (
                  // as the link is hover over, we have to pass onHover down to that element
                  <HeaderItem
                    mouseEvents={mouseEvents}
                    key={phrase}
                    value={phrase}
                  />
                );
              } else
                return (
                  <Link key="logo" to={`/`}>
                    <img alt="header logo" src={logo} className="topLogo" />
                  </Link>
                );
            })}
          </div>
        </div>
        <Dropdown mouseEvents={() => this.handleHover()} />
      </div>
    );
  }
}
