import React from "react";
import ReactDOM from "react-dom";

import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./components/Header.js";
import Footer from "./components/Footer.js";

import HomePage from "./pages/Home.js";
import Classes from "./pages/Classes.js";

const App = () => {
  return (
    <div>
      <Router>
        <Header />
        <Route exact path="/" component={HomePage}></Route>
        <Route exact path="/classes" component={Classes}></Route>
        <Footer />
      </Router>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
